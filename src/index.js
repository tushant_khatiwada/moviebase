import { Router, Route, browserHistory } from 'react-router'
import ReactDOM from 'react-dom';
import React from 'react';

import 'react-mdl/extra/material.css';
import 'react-mdl/extra/material.js';

import MovieDetailView from './component/MovieDetailView';
import DetailLayout from './component/DetailLayout';
import App from './App';
import './index.css';

ReactDOM.render((
  <Router history={browserHistory}>
    <Route path="/" component={App} >
    </Route>
    <Route path="detail" component={DetailLayout}>
      <Route path="movie/:id" component={MovieDetailView} />
    </Route>
  </Router>
), document.getElementById('root'));
