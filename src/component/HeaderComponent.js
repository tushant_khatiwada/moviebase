import React from 'react';
import _ from 'lodash';
import {HeaderRow, IconButton} from 'react-mdl';


const ShowInfoHeader = (movie, mainCastJoin) => {
  return(
    <div>
      <HeaderRow title={movie.title}>
        <IconButton name="more_vert" id="demo-menu-lower-left" />
      </HeaderRow>
      <HeaderRow>
        <i className="material-icons">date_range</i>&nbsp;{mainCastJoin}
      </HeaderRow>
      <HeaderRow>
        <i className="material-icons">history</i>&nbsp;55 episodes/ 4 seasons/ 2013 -
      </HeaderRow>
    </div>
  );
}

const EpisodeHeader = (movie) => {
  return(
    <div>
      <HeaderRow title={movie.title}>
        <IconButton name="more_vert" id="demo-menu-lower-left" />
      </HeaderRow>
      <HeaderRow>
        <i className="material-icons">date_range</i>&nbsp;episode
      </HeaderRow>
      <HeaderRow>
        <i className="material-icons">history</i>&nbsp;55 episodes/ 4 seasons/ 2013 -
      </HeaderRow>
    </div>
  );
}

const CastHeader = (movie) => {
  return(
    <div>
      <HeaderRow title={movie.title}>
        <IconButton name="more_vert" id="demo-menu-lower-left" />
      </HeaderRow>
      <HeaderRow>
        <i className="material-icons">date_range</i>&nbsp;main cast
      </HeaderRow>
      <HeaderRow>
        <i className="material-icons">history</i>&nbsp;55 episodes/ 4 seasons/ 2013 -
      </HeaderRow>
    </div>
  );
}

const HeaderComponent = ({activeTab, movie}) => {
  const mainCastJoin = _.join(movie.mainCast, ', ');

  if(activeTab===0){
  return(
    <div>{ShowInfoHeader(movie, mainCastJoin)}</div>
  )
}
if(activeTab===1){
  return(
    <div>{EpisodeHeader(movie)}</div>
  )
}
if(activeTab===2){
  return(
    <div>{CastHeader(movie)}</div>
  )
}
}

export default HeaderComponent;
