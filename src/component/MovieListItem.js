import React from 'react';
import _ from 'lodash';
import {Link} from 'react-router';

import imdb from '../../public/img/images.jpg';
import rottenTomatoes from '../../public/img/fresh.png';

const MovieListItem = ({movie}) => {
  const imageUrl = movie.imageUrl;
  const mainCastJoin = _.join(movie.mainCast, ', ');
  return (
    <li className="list-group-item">
        <div className="video-list media">
          <div className="media-left">
            <img className="media-object" src={imageUrl} alt={movie.title} />
          </div>
          <div className="media-body">
            <div className="media-heading">
            <Link to={"detail/movie/" + movie.id}><h4 className="title">{movie.title}</h4></Link>
            </div>
            <div className="main-cast">
              <ul id="cast-list">
                  <li className="list-item">
                    {mainCastJoin}...
                  </li>
              </ul>
            </div>
            <div className="reviewer">
              <img className="img-responsive reviewer-img" src={imdb} alt="{movie.title}" />
              <div className="imdbScore">
                {movie.imdb}
              </div>
              <img className="img-responsive reviewer-img" src={rottenTomatoes} alt="{movie.title}" />
              <div style={{verticalAlign:'middle', display:'inline'}} className="rottenTomatoesScore">
                {movie.rottenTomatoes}
              </div>
            </div>
          </div>
        </div>
      </li>
      )
};

export default MovieListItem;
