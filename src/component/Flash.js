import React, {Component} from 'react';
import {Link} from 'react-router';
import {Header, HeaderRow,HeaderTabs, Tab, Layout, Content, IconButton } from 'react-mdl';

export default class Flash extends Component {
  constructor(props){
    super(props);
    this.state = {activeTab: 2};
  }
  render() {
    return (
      <div className="demo-big-content">
        <Layout fixedHeader fixedTabs>
            <Header>
                <HeaderRow title="Flash">
                  <IconButton name="more_vert" id="demo-menu-lower-left" />
                </HeaderRow>
                <HeaderRow>
                  John, Jane
                </HeaderRow>
                <HeaderRow>
                  55 episodes/ 4 seasons/ 2013 -
                </HeaderRow>
                <HeaderTabs ripple activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId })}>
                    <Tab>SHOW INFO</Tab>
                    <Tab>EPISODES</Tab>
                    <Tab>CAST</Tab>
                </HeaderTabs>
                  <Link to="/"><i className="material-icons arrow_back">arrow_back</i></Link>
            </Header>
            <Content>
                <div className="page-content">Content for the tab: {this.state.activeTab}</div>
            </Content>
          </Layout>
        </div>

    );
  }
}
