import React from 'react';

import _ from 'lodash';

import MovieListItem from './MovieListItem';

const MovieList = ({movies}) => {
    const MovieItems = _.map(movies, (movie) => {
        return <MovieListItem key={movie.id} movie={movie} />
    });
    return (
        <ul className="col-md-4 list-group">
            { MovieItems }
        </ul>
    );
};
export default MovieList;
