import React, {Component} from 'react';
import {Link} from 'react-router';

import $ from 'jquery';
import _ from 'lodash';

import {Header, HeaderTabs, Tab, Layout, Content } from 'react-mdl';

import HeaderComponent from './HeaderComponent';
import MovieInfo from './MovieInfo';



class MovieDetailView extends Component{
  constructor(props){
    super(props);
    this.state = {
      movie:[ ],
      activeTab:1
    }
  }

  fetchSingleMovieFromServer(){
    let id = this.props.params.id;
    $.ajax({
      url: '/api/movie'+id+'.json',
      dataType: 'json',
      success: (data) => {
         this.setState({movie: data['movie']});
       },
      error: (xhr, status, err) => {
        console.error(this.url, status, err.toString());
      }
    });
  }

  componentDidMount(){
    this.fetchSingleMovieFromServer();
  }

  render(){
    if(!this.state.movie){
      return(
        <p>Loading...</p>
      );
    }
    return(
      <div className="demo-big-content">
        <Layout fixedHeader fixedTabs>
            <Header>
              {this.state.activeTab >= 0 ? <HeaderComponent movie={this.state.movie} activeTab={this.state.activeTab}/> : ' '}
                <HeaderTabs ripple activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId })}>
                    <Tab>SHOW INFO</Tab>
                    <Tab>EPISODES</Tab>
                    <Tab>CAST</Tab>
                </HeaderTabs>
                  <Link to="/"><i className="material-icons arrow_back">arrow_back</i></Link>
            </Header>
            <Content>
                <div className="page-content">{this.state.activeTab >=0 ? <MovieInfo movie={this.state.movie} activeTab={this.state.activeTab} /> : ''}</div>
            </Content>
          </Layout>
        </div>
    );
  }
}

export default MovieDetailView;
