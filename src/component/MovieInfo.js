import React, {Component} from 'react';

import _ from 'lodash';

import {Card, List, ListItem, ListItemContent} from 'react-mdl';

import rottenTomatoes from '../../public/img/fresh.png';
import imdb from '../../public/img/images.jpg';

export default class MovieInfo extends Component {
  renderInfo(){
    let movie = this.props.movie;
    const image = movie.imageUrl;
    return(
      <div>
        <Card shadow={0}
              style={{width: '100%', height: '300px', background: 'url('+ image +') top / cover', margin: 'auto'}}>
        </Card>
        <div className="flex-box">
          <ul className="flex-container">
            <div className="flex-item">
              <li className="genre">Genre</li>
              <p className="sub">{movie.genre}</p>
            </div>
            <div className="flex-item">
                <li className="runtime">Runtime</li>
                <p className="sub">{movie.runtime}</p>
            </div>
          </ul>
        </div>
          <div className="reviewer-flex-container">
            <li className="flex-items"><img className="img-responsive" src={imdb} alt="{movie.title}" /></li>
            <li className="flex-items">{movie.imdb}</li>
            <li className="flex-items"><img className="img-responsive rottenImage" src={rottenTomatoes} alt="{movie.title}" /></li>
            <li className="flex-items">{movie.rottenTomatoes}</li>
        </div>
        <hr/>
        <div className="description">
          <p className="text-flow">{movie.description}</p>
        </div>
      </div>
    );
  }

  renderEpisode(){
    let movie = this.props.movie;
    console.log('episode', movie.episodes);
    let episode = _.map(movie.episodes, (episode) => {
      return(
        <div key={episode.episodeName} className="flex-container-1">
          <div className="flex-item-1">
            <img src={episode.imageUrl} alt={episode.episodeName} className="img-responsive" width="100" height="50"/>
          </div>
          <div className="flex-container-2">
            <div className="flex-item-2 episodeName">
              {episode.episodeName}
            </div>
            <div className="flex-item-2 time">
              {episode.time}
            </div>
          </div>
        </div>
      );
    });
    return(
      <div className="epsiodes">
        {episode}
      </div>
    );
  }

  renderCast(){
    let movie = this.props.movie;
    let cast = _.map(movie.fullCast, (cast, index) => {
      return(
        <List key={index} style={{width: '300px'}}>
          <ListItem twoLine>
            <ListItemContent avatar={<img src={movie.imageUrl} role="presentation"/>} subtitle={cast.nickname}>{cast.name}</ListItemContent>
          </ListItem>
        </List>
      );
    });
    return(
      <div className="cast">
        {cast}
      </div>
    );
  }
  render() {
    if(this.props.activeTab===0){
      return this.renderInfo();
    }

    if(this.props.activeTab===1){
      return this.renderEpisode();
    }

    if(this.props.activeTab===2){
      return this.renderCast();
    }
  }
}
