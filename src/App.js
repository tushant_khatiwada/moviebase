import React, { Component } from 'react';
import $ from 'jquery';

import {Header, HeaderRow, Layout, Drawer, Content } from 'react-mdl';

import MovieList from './component/MovieList';
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      movies:[ ]
    }
  }

  fetchDataFromServer(){
    $.ajax({
     url: 'api/movie.json',
     dataType: 'json',
     cache: false,
     success: (data) => {
        this.setState({movies: data['movies']});
      },
     error: (xhr, status, err) => {
       console.error(this.url, status, err.toString());
     }
   });
  }

  componentDidMount(){
    this.fetchDataFromServer();
  }

  render() {
    return (
      <div className="App">
        <div className="demo-big-content">
            <Layout fixedHeader>
                <Header>
                    <HeaderRow title="Find TV Shows">
                    </HeaderRow>
                    <HeaderRow>
                      <div className="search">
                        <span className="material-icons search-icon">search</span>
                        <input type="text" className="form-control" placeholder="Name of show(e.g Friends)" />
                      </div>
                    </HeaderRow>
                </Header>
                <Drawer title="Title">
                </Drawer>
                <Content>
                    <div className="page-content" />
                    <MovieList movies={this.state.movies} />
                </Content>
            </Layout>
          </div>
      </div>
    );
  }
}

export default App;
